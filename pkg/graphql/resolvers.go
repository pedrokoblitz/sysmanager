package graphql

import (
	"context"
	"encoding/base64"
	"strings"

	"gitlab.com/pedrokoblitz/sysmanager/pkg/stl" // replace with your actual module name
	"gitlab.com/pedrokoblitz/sysmanager/graph/generated"
)

type Resolver struct {
	stlService *stl.STLService
}

func NewResolver(stlService *stl.STLService) *Resolver {
	return &Resolver{stlService: stlService}
}

func (r *Resolver) Mutation() generated.MutationResolver {
	return &mutationResolver{r}
}

type mutationResolver struct{ *Resolver }

func (r *mutationResolver) ProcessFile(ctx context.Context, input generated.ProcessFileInput) (*generated.ProcessFileResponse, error) {
	data, err := base64.StdEncoding.DecodeString(input.FileContent)
	if err != nil {
		return nil, err
	}

	reader := strings.NewReader(string(data))
	frontendData, err := r.stlService.ProcessStream(reader)
	if err != nil {
		return nil, err
	}

	jsonData, err := stl.ExportToJSON(frontendData)
	if err != nil {
		return nil, err
	}

	return &generated.ProcessFileResponse{Data: jsonData}, nil
}

func (r *mutationResolver) ProcessStream(ctx context.Context, input generated.ProcessStreamInput) (*generated.ProcessStreamResponse, error) {
	data, err := base64.StdEncoding.DecodeString(input.FileContent)
	if err != nil {
		return nil, err
	}

	reader := strings.NewReader(string(data))
	triangleCh := make(chan stl.Triangle, 100)
	var wg sync.WaitGroup
	wg.Add(1)

	go r.stlService.ParseSTLStream(reader, triangleCh, &wg)

	go func() {
		wg.Wait()
		close(triangleCh)
	}()

	var triangles []*generated.Triangle
	var totalArea float64
	for triangle := range triangleCh {
		area := stl.AreaOfTriangle(triangle)
		totalArea += area

		triangles = append(triangles, &generated.Triangle{
			V1: &generated.Vertex{X: triangle.V1.X, Y: triangle.V1.Y, Z: triangle.V1.Z},
			V2: &generated.Vertex{X: triangle.V2.X, Y: triangle.V2.Y, Z: triangle.V2.Z},
			V3: &generated.Vertex{X: triangle.V3.X, Y: triangle.V3.Y, Z: triangle.V3.Z},
		})
	}

	return &generated.ProcessStreamResponse{
		Triangles:        triangles,
		CurrentTotalArea: totalArea,
	}, nil
}
