package graphql

import (
	"log"
	"net/http"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"gitlab.com/pedrokoblitz/sysmanager/graph/generated"
	"gitlab.com/pedrokoblitz/sysmanager/pkg/stl"
)

func StartGraphQLServer(address string, stlService *stl.STLService) {
	resolver := NewResolver(stlService)
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: resolver}))

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", srv)

	log.Printf("connect to http://localhost%s/ for GraphQL playground", address)
	log.Fatal(http.ListenAndServe(address, nil))
}
