package grpc

import (
	"log"
	"net"

	"gitlab.com/pedrokoblitz/sysmanager/proto"
	"google.golang.org/grpc"
)

func StartGRPCServer(address string, stlService *stl.STLService) {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	server := grpc.NewServer()
	proto.RegisterSTLServiceServer(server, NewGRPCServer(stlService))

	log.Printf("gRPC server is listening on %s", address)
	if err := server.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
