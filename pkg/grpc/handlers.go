package grpc

import (
	"context"
	"encoding/base64"
	"fmt"
	"strings"
	"sync"

	"gitlab.com/pedrokoblitz/sysmanager/pkg/stl" // replace with your actual module name
	"gitlab.com/pedrokoblitz/sysmanager/proto"
)

type grpcServer struct {
	proto.UnimplementedSTLServiceServer
	stlService *stl.STLService
}

func NewGRPCServer(stlService *stl.STLService) *grpcServer {
	return &grpcServer{stlService: stlService}
}

func (s *grpcServer) ProcessFile(ctx context.Context, req *proto.ProcessFileRequest) (*proto.ProcessFileResponse, error) {
	data, err := base64.StdEncoding.DecodeString(req.FileContent)
	if err != nil {
		return nil, fmt.Errorf("invalid base64 data")
	}

	reader := strings.NewReader(string(data))
	frontendData, err := s.stlService.ProcessStream(reader)
	if err != nil {
		return nil, err
	}

	jsonData, err := stl.ExportToJSON(frontendData)
	if err != nil {
		return nil, fmt.Errorf("failed to convert to JSON")
	}

	return &proto.ProcessFileResponse{Data: jsonData}, nil
}

func (s *grpcServer) ProcessStream(req *proto.ProcessStreamRequest, stream proto.STLService_ProcessStreamServer) error {
	data, err := base64.StdEncoding.DecodeString(req.FileContent)
	if err != nil {
		return fmt.Errorf("invalid base64 data")
	}

	reader := strings.NewReader(string(data))
	triangleCh := make(chan stl.Triangle, 100)
	var wg sync.WaitGroup
	wg.Add(1)

	go s.stlService.ParseSTLStream(reader, triangleCh, &wg)

	go func() {
		wg.Wait()
		close(triangleCh)
	}()

	var totalArea float64
	for triangle := range triangleCh {
		area := stl.AreaOfTriangle(triangle)
		totalArea += area

		triangleProto := &proto.Triangle{
			V1: &proto.Vertex{X: triangle.V1.X, Y: triangle.V1.Y, Z: triangle.V1.Z},
			V2: &proto.Vertex{X: triangle.V2.X, Y: triangle.V2.Y, Z: triangle.V2.Z},
			V3: &proto.Vertex{X: triangle.V3.X, Y: triangle.V3.Y, Z: triangle.V3.Z},
		}
		stream.Send(&proto.ProcessStreamResponse{
			Triangles:        []*proto.Triangle{triangleProto},
			CurrentTotalArea: totalArea,
		})
	}

	return nil
}
