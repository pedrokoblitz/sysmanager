package stl

// Vertex represents a point in 3D space
type Vertex struct {
	X, Y, Z float64
}

// Triangle represents a triangle in 3D space with three vertices
type Triangle struct {
	V1, V2, V3 Vertex
}

// FrontendData represents the data structure forwarded to the frontend
type FrontendData struct {
	Vertices  []Vertex
	Triangles []Triangle
}
