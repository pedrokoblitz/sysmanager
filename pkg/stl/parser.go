package stl

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"sync"
)

// parseSTLFile parses an STL file and sends triangles to a channel
func parseSTLFile(filePath string, triangleCh chan<- Triangle, wg *sync.WaitGroup) {
	defer wg.Done()

	file, err := os.Open(filePath)
	if err != nil {
		fmt.Println("Failed to open file:", err)
		close(triangleCh)
		return
	}
	defer file.Close()

	parseSTLStream(file, triangleCh, wg)
}

// parseSTLStream parses an STL stream and sends triangles to a channel
func parseSTLStream(reader io.Reader, triangleCh chan<- Triangle, wg *sync.WaitGroup) {
	defer wg.Done()

	scanner := bufio.NewScanner(reader)
	var vertices []Vertex
	for scanner.Scan() {
		line := scanner.Text()
		tokens := strings.Fields(line)
		if len(tokens) == 0 {
			continue
		}

		switch tokens[0] {
		case "vertex":
			x, y, z := parseVertex(tokens[1:])
			vertices = append(vertices, Vertex{x, y, z})
		case "endloop":
			if len(vertices) == 3 {
				triangleCh <- Triangle{vertices[0], vertices[1], vertices[2]}
				vertices = vertices[:0] // reset vertices
			}
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Println("Failed to read stream:", err)
	}
	close(triangleCh)
}

// parseVertex converts string slice to a Vertex
func parseVertex(data []string) (float64, float64, float64) {
	x := parseFloat(data[0])
	y := parseFloat(data[1])
	z := parseFloat(data[2])
	return x, y, z
}

// parseFloat converts string to float64
func parseFloat(s string) float64 {
	v, err := strconv.ParseFloat(s, 64)
	if err != nil {
		fmt.Println("Error parsing float:", err)
	}
	return v
}
