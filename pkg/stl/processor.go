package stl

import (
	"math"
	"sync"
)

// processTriangles processes triangles to prepare data for frontends
func processTriangles(triangleCh <-chan Triangle, frontendDataCh chan<- FrontendData, wg *sync.WaitGroup) {
	defer wg.Done()
	var vertices []Vertex
	var triangles []Triangle

	for triangle := range triangleCh {
		vertices = append(vertices, triangle.V1, triangle.V2, triangle.V3)
		triangles = append(triangles, triangle)
	}

	frontendDataCh <- FrontendData{
		Vertices:  vertices,
		Triangles: triangles,
	}
	close(frontendDataCh)
}

// areaOfTriangle calculates the area of a triangle given by three vertices
func areaOfTriangle(t Triangle) float64 {
	// Vector from V1 to V2
	u1 := t.V2.X - t.V1.X
	u2 := t.V2.Y - t.V1.Y
	u3 := t.V2.Z - t.V1.Z

	// Vector from V1 to V3
	v1 := t.V3.X - t.V1.X
	v2 := t.V3.Y - t.V1.Y
	v3 := t.V3.Z - t.V1.Z

	// Cross product of u and v
	cx := u2*v3 - u3*v2
	cy := u3*v1 - u1*v3
	cz := u1*v2 - u2*v1

	// Area of the parallelogram is the magnitude of the cross product
	area := math.Sqrt(cx*cx + cy*cy + cz*cz)

	// The area of the triangle is half the area of the parallelogram
	return area / 2
}
