package stl

import (
	"encoding/json"
	"fmt"
)

// ExportToJSON converts FrontendData to a JSON string
func ExportToJSON(data FrontendData) (string, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return "", fmt.Errorf("failed to marshal data to JSON: %v", err)
	}
	return string(jsonData), nil
}
