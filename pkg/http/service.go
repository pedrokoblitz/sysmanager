package http

import (
	"gitlab.com/pedrokoblitz/sysmanager/pkg/stl" // replace 'your_module_name' with the actual module name
	"io"
	"sync"
)

// STLService provides methods for handling STL-related HTTP requests
type STLService struct {
	stlProcessor *stl.STLService
	numWorkers   int
}

// NewSTLService creates a new STLService
func NewSTLService(numWorkers int) *STLService {
	return &STLService{
		stlProcessor: stl.NewSTLService(),
		numWorkers:   numWorkers,
	}
}

// ProcessStream processes STL data from an io.Reader and returns data ready for frontends
func (s *STLService) ProcessStream(reader io.Reader) (stl.FrontendData, error) {
	triangleCh := make(chan stl.Triangle, 100)
	frontendDataCh := make(chan stl.FrontendData)
	var wg sync.WaitGroup

	wg.Add(1)
	go s.stlProcessor.ParseSTLStream(reader, triangleCh, &wg)

	wg.Add(1)
	go s.stlProcessor.ProcessTriangles(triangleCh, frontendDataCh, &wg)

	go func() {
		wg.Wait()
		close(frontendDataCh)
	}()

	return <-frontendDataCh, nil
}
