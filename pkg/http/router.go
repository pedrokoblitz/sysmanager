package http

import (
	"github.com/gin-gonic/gin"
)

// SetupRouter initializes the Gin router with the appropriate routes
func SetupRouter(stlService *STLService) *gin.Engine {
	r := gin.Default()
	r.POST("/process/batched", stlService.BatchedHandler)
	r.POST("/process/streaming", stlService.StreamingHandler)
	return r
}
