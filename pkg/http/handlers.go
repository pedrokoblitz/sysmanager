package http

import (
	"encoding/base64"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

// STLRequest represents the incoming request payload
type STLRequest struct {
	FileContent string `json:"file_content"`
}

// BatchedHandler handles batched processing of STL files
func (s *STLService) BatchedHandler(c *gin.Context) {
	var req STLRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	data, err := base64.StdEncoding.DecodeString(req.FileContent)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid base64 data"})
		return
	}

	reader := strings.NewReader(string(data))
	frontendData, err := s.ProcessStream(reader)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	jsonData, err := ExportToJSON(frontendData)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "failed to convert to JSON"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": jsonData})
}

// StreamingHandler handles streaming processing of STL files
func (s *STLService) StreamingHandler(c *gin.Context) {
	var req STLRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid request"})
		return
	}

	data, err := base64.StdEncoding.DecodeString(req.FileContent)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid base64 data"})
		return
	}

	reader := strings.NewReader(string(data))
	triangleCh := make(chan Triangle, 100)
	var wg sync.WaitGroup
	wg.Add(1)

	go parseSTLStream(reader, triangleCh, &wg)

	go func() {
		wg.Wait()
		close(triangleCh)
	}()

	var totalArea float64
	for triangle := range triangleCh {
		area := areaOfTriangle(triangle)
		totalArea += area
		c.SSEvent("triangle", gin.H{"triangle": triangle, "current_total_area": totalArea})
	}

	c.JSON(http.StatusOK, gin.H{"total_surface_area": totalArea})
}
