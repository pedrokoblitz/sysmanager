package http

// STLRequest represents the incoming request payload
type STLRequest struct {
	FileContent string `json:"file_content"`
}
