# Variables
GIT_USER = pedrokoblitz
APP_NAME = sysmanager
DOCKER_IMAGE = $(APP_NAME)
DOCKER_COMPOSE_FILE = docker-compose.yml
AIR_CONFIG = air.toml

# Default target
.PHONY: all
all: build

# Build the Docker image
.PHONY: build
build:
	docker build -t $(DOCKER_IMAGE) .

# Run the application with Docker Compose
.PHONY: up
up:
	docker-compose -f $(DOCKER_COMPOSE_FILE) up --build

# Stop the Docker Compose services
.PHONY: down
down:
	docker-compose -f $(DOCKER_COMPOSE_FILE) down

# Restart the Docker Compose services
.PHONY: restart
restart: down up

# Run tests
.PHONY: test
test:
	go test ./...

# Clean up generated files
.PHONY: clean
clean:
	rm -rf tmp/

# Hot reload with Air
.PHONY: air
air:
	air -c $(AIR_CONFIG)

# Run the application without Docker (for local development)
.PHONY: run
run:
	go run cmd/app/main.go

# Ensure dependencies are installed
.PHONY: deps
deps:
	go mod tidy

# Initialize the project (Go module and dependencies)
.PHONY: init
init:
	go mod init github.com/$(GIT_USER)/$(APP_NAME)
	go mod tidy
