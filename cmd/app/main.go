package main

import (
	"log"
	"sync"

	"github.com/gin-gonic/gin"
	"gitlab.com/pedrokoblitz/sysmanager/pkg/graphql" // replace 'your_module_name' with the actual module name
	"gitlab.com/pedrokoblitz/sysmanager/pkg/grpc"
	"gitlab.com/pedrokoblitz/sysmanager/pkg/http"
	"gitlab.com/pedrokoblitz/sysmanager/pkg/stl"
	"gitlab.com/pedrokoblitz/sysmanager/pkg/websocket"
	"github.com/swaggo/files" // swagger embed files
	"github.com/swaggo/gin-swagger" // gin-swagger middleware
	_ "gitlab.com/pedrokoblitz/sysmanager/docs" // Import generated docs
)

// @title MyGoProject API
// @version 1.0
// @description This is a sample server for MyGoProject.
// @host localhost:8080
// @host localhost:8081
// @host localhost:50051
// @BasePath /

func main() {
	stlService := stl.NewSTLService()
	hub := websocket.NewHub()

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		httpRouter := http.SetupRouter(http.NewSTLService(8))
		httpRouter.GET("/ws", websocket.WebSocketHandler(hub))
		// Swagger API docs
		httpRouter.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		if err := httpRouter.Run(":8080"); err != nil {
			log.Fatalf("HTTP server failed: %v", err)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		grpc.StartGRPCServer(":50051", stlService)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		graphql.StartGraphQLServer(":8081", stlService)
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		hub.Run()
	}()

	wg.Wait()
}
