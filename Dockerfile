# Start from the official Go image
FROM golang:1.20-alpine AS builder

# Set the Current Working Directory inside the container
WORKDIR /app

# Install necessary packages
RUN apk add --no-cache git curl

# Download Air for hot reloading
RUN curl -fLo /bin/air https://raw.githubusercontent.com/cosmtrek/air/master/bin/linux/air && \
    chmod +x /bin/air

# Copy go mod and go sum files
COPY go.mod go.sum ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download

# Copy the source from the current directory to the Working Directory inside the container
COPY . .

# Expose port 8080 to the outside world
EXPOSE 8080
EXPOSE 8081
EXPOSE 50051

# Command to run Air
CMD ["air"]
